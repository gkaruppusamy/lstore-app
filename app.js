'use strict';

// Declare app level module which depends on views, and components
angular.module('lStore', [
  'ngRoute',
  'ngResource',
  '720kb.socialshare',
  'myApp.version'
])

// Module configuration
.config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
  

  $routeProvider.when('/', {
    templateUrl: 'view/list.html',
    controller: 'ListController'
  })
  .when('/detail/:id', {
    templateUrl: 'view/detail.html',
    controller: 'DetailController'
  })
  .when('/update', {
    templateUrl: 'view/update.html',
    controller: 'UpdateController'
  })
  .when('/update/:id', {
    templateUrl: 'view/update.html',
    controller: 'UpdateController'
  })
  .otherwise({redirectTo: '/'});

  $locationProvider.html5Mode(true);
  // API key
  $httpProvider.defaults.headers.common['X-Authorization']= 'f8e4e455fa12f27e76f44bb03ec8ede2d8af805f';
}])


.run(function($rootScope, $location) {
	$rootScope.$on('addMessages', function(event, data){
		$rootScope.messages = data.messages;
		$rootScope.messageType = data.type;
	});

	$rootScope.location = $location;
})

// Book service
.factory('book', function($resource) {
  return $resource('http://localhost/test/laravel/lstore/public/api/book/:id', null, {
        'update': { method:'PUT' }
    });
})

// Author service
.factory('author', function($resource) {
  return $resource('http://localhost/test/laravel/lstore/public/api/term/authors');
})

// Genre service
.factory('genre', function($resource) {
  return $resource('http://localhost/test/laravel/lstore/public/api/term/genres');
})

// List controller
.controller('ListController', ['$scope', 'book', function($scope, book) {
	book.get({}, function(books) {
		$scope.books = books.data;
	});
}])

// Detail controller
.controller('DetailController', ['$scope', 'book', '$routeParams', function($scope, book, $routeParams) {
	book.get({id:$routeParams.id}, function(book) {
		$scope.book = book.data;
	});

	$scope.pdf = function() {
		var printDoc = new jsPDF();

		var source = window.document.getElementsByTagName("body")[0];
		printDoc.fromHTML(source, 10, 10, {'width': 180});
	    //printDoc.autoPrint();
	    //printDoc.output("dataurlnewwindow");
	    printDoc.save($scope.book.name +" Book.pdf");
	};
}])

// Book add and update controller
.controller('UpdateController', ['$scope', 'author', 'genre', '$routeParams', 'book', '$rootScope', function($scope, author, genre, $routeParams, bookSer, $rootScope) {
	author.get({}, function(author) {
		$scope.authors = author.data;
	});

	genre.get({}, function(genre) {
		$scope.genres = genre.data;
	});

	$scope.master = {};
	
	if (!angular.isDefined($routeParams.id)) {
		$scope.book = {};
		$scope.action = 'Add';
	} else {
		bookSer.get({id:$routeParams.id}, function(resBook) {
			$scope.book = resBook.data;
		});
		$scope.action = 'Update';
	}

	$scope.update = function(isValid, book) {

		if (!isValid)
			return false;

		book.entity_type_id = 1; // For book

		var bookRes = new bookSer();
		bookRes = angular.copy(book, bookRes);

		if ($scope.action == 'Add') {
			bookRes.$save(function(data){
				$scope.$emit('addMessages', {'messages': data.meta.messages, 'type': 'success'});
			}, function(error) {
				//console.log(error);
				$scope.$emit('errorMessages', {'messages': error.data.meta.messages, 'type': 'danger'});
			});
		} else {
			bookRes.$update({id: book.id}, function(data){
				$scope.$emit('addMessages', {'messages': data.meta.messages, 'type': 'success'});
			}, function(error) {
				//console.log(error);
				$scope.$emit('errorMessages', {'messages': error.data.meta.messages, 'type': 'danger'});
			});
		}
	}
}]);

